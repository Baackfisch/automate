package de.Unknown;

import java.util.ArrayList;
import java.util.List;

public class Automate {

// Automate ------------------------------------------------------------------------------------------------------------

    private List<State> states;
    private List<String> alphabet;
    private int state = 0;

    public Automate(){

        states = new ArrayList<>();
        alphabet = new ArrayList<>();
    }

// Methods for the Automate --------------------------------------------------------------------------------------------

    public void addState(String id){
        states.add(new State(id));
    }

    public void addAlphabetItem(String id){
        alphabet.add(id);
    }

    public void addStateSwitch(String alphabetID,int baseID, int nID){ // nID for next ID
        states.get(baseID).inputToNext.put(alphabetID,nID);
    }
    public boolean isAlphabet(String inAlphabet){
        return alphabet.contains(inAlphabet);
    }
    public int posAlphabet(String inAlphabet){
        if(isAlphabet(inAlphabet)){
            for (int i = 0; i < alphabet.size(); i++) {
                if( alphabet.get(i).equals(inAlphabet) ) return i;
            }
        }
        return -1;
    }
    public int stateSize(){
        return states.size();
    }

// running this automate -----------------------------------------------------------------------------------------------

    public boolean runStep(String inAlphabet){
        if (isAlphabet(inAlphabet)) // no brackets needed
            try {
                state = states.get(state).inputToNext.get(posAlphabet(inAlphabet)).intValue();
                return true;
            } catch (NullPointerException n) {
                return false;
            }
        return false;
    }
    public boolean runCompletly(String ...s){
        boolean b =true;
        for (String step : s) {
            if (!runStep(step)) b = false;
        }
        return b;
    }

// main ----------------------------------------------------------------------------------------------------------------

    public static void main(String[] args) {
        new Frame(new Automate());

    }
}
