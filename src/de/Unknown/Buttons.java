package de.Unknown;

import javax.swing.*;
import java.awt.*;

public class Buttons extends JButton {
    public Buttons(String text){
        this.setText(text);
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setFocusable(false);
        this.setContentAreaFilled(false);
        this.setBorderPainted(false);
    }
    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(new Color(200,200,200));
        g.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
    }
}
