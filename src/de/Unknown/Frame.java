package de.Unknown;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Frame extends JFrame {

    public JPanel automate, tools;
    public Buttons addState , addAlphabet;
    public TextField stateName, alphabetName;
    public ButtonGroup alphabetItems;

    private List<JRadioButton> radioButtons;

    private ImageIcon i;

    @Override
    public JLayeredPane getLayeredPane() {
        return super.getLayeredPane();
    }

    //Frame itself ---------------------------------------------------------------------------------------------------------
    public Frame(Automate auto){
        this.setSize(1006,800);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setLayout(null);
        this.setResizable(false);
        init(auto);
    }
//JComponents ----------------------------------------------------------------------------------------------------------
    private void init(Automate a) {
        //Radiobuttonlist
        radioButtons = new ArrayList<>();

        // automatePanel
        automate = new JPanel();
        automate.setSize(800,800);
        automate.setBackground(Color.GRAY);
        automate.setLocation(0,0);
        automate.setLayout(null);

        // Buttongroup
        alphabetItems = new ButtonGroup();

        // buttonsList
        buttons = new ArrayList<>();

        // toolsPanel
        tools = new JPanel();
        tools.setSize(200,800);
        tools.setLocation(800,0);
        tools.setLayout(null);

        // addStateNameTextfield
        stateName = new TextField(40);
        stateName.setSize(200,20);
        stateName.setLocation(0,10);

        //addAlphabetTextfield
        alphabetName = new TextField(40);
        alphabetName.setSize(200,20);
        alphabetName.setLocation(0,130);

        // addStateButton
        addState = new Buttons("Add a new State");
        addState.setSize(200,80);
        addState.setLocation(0,40);

        addState.addActionListener(e -> {
            if(!stateName.getText().equals("")){
                a.addState(stateName.getText());
                stateName.setText("");
                updateAutomatePanel(a);
            }
        });

        // addAlphabetButton
        addAlphabet = new Buttons("Add a new AlphabetItem");
        addAlphabet.setSize(200,80);
        addAlphabet.setLocation(0,155);

        addAlphabet.addActionListener(e -> {
            if(!alphabetName.getText().equals("")){
                JRadioButton r = new JRadioButton(alphabetName.getText());
                r.setLocation(0,240 + alphabetItems.getButtonCount() * 20);
                r.setSize(200,20);
                alphabetItems.add(r);
                radioButtons.add(r);
                tools.add(r);
                tools.repaint();
                alphabetName.setText("");
            }
        });

        //add everything to his Parent
        tools.add(alphabetName);
        tools.add(addAlphabet);
        tools.add(stateName);
        tools.add(addState);
        this.add(automate);
        this.add(tools);
        this.validate();

        //Image of the States
        i = new ImageIcon("assets/Button.png");
        i.setImage(i.getImage().getScaledInstance(50,50,0));
    }

    //Automate Panel
    private List<JButton> buttons;
    private int firstPressed = -1;

    private void updateAutomatePanel(Automate a){
        automate.removeAll();
        double alpha = 360 / a.stateSize();
        double radius = (75 /(Math.sin(Math.PI/a.stateSize())*2));
        radius = radius > 100000 ? 0 : radius;


        //new Button
        JButton b = new JButton();
        b.setActionCommand(Integer.toString(buttons.size()));
        b.setIcon(i);
        b.setContentAreaFilled(false);
        b.setBorderPainted(false);
        b.setFocusable(false);
        b.setHideActionText(true);
        b.setSize(50,50);
        b.setLocation(175,155);

        b.addActionListener(e -> {
            String[] params = e.paramString().split(",");
            int buttonPressed = -1;
            for(String param : params){
                if( param.substring(0,4).equals("cmd=")){
                    buttonPressed = Integer.parseInt(param.substring(4,param.length()));
                    break;
                }
            }
            if(firstPressed < 0){
                firstPressed = buttonPressed;
            } else {
                int radiobuttons = 0;
                while (radiobuttons < radioButtons.size()) {
                    if(radioButtons.get(radiobuttons).isSelected()) break;
                    radiobuttons++;
                }

                a.addStateSwitch(radioButtons.get(radiobuttons).getText(), firstPressed, buttonPressed);
                try {
                    automate.add(arrawFromTo(buttons.get(firstPressed).getX(),buttons.get(firstPressed).getY(),buttons.get(buttonPressed).getX(),buttons.get(buttonPressed).getY()));
                    System.out.println("Baum");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                System.out.println("New Switch created for" + firstPressed + ":" + buttonPressed +":"+radioButtons.get(radiobuttons).getText());
                firstPressed = -1;
            }
            automate.repaint();
            System.out.println(buttonPressed);



        });

        buttons.add(b);
        for(int i = 1; i <= a.stateSize(); i++){
            buttons.get(i-1).setLocation(
                    (int) (Math.cos(Math.toRadians(alpha*i)) * radius) + automate.getWidth() / 2 - 25,
                    (int) (Math.sin(Math.toRadians(alpha*i)) * radius) + automate.getHeight()/2 - 40);
            automate.add( buttons.get(i-1) );
            automate.validate();
            automate.repaint();
        }
    }

    private JLabel arrawFromTo(int x0,int y0, int x1, int y1) throws IOException {
        JLabel arrawLabel;
        double arrawLenght = Math.sqrt(Math.pow(x1-x0,2) + Math.pow(y1-y0,2));
        BufferedImage bi = ImageIO.read(new File("assets/Pfeil.png"));
        Graphics2D g = ((Graphics2D)bi.getGraphics());
        g.rotate(Math.PI);
        arrawLabel = new JLabel();

        arrawLabel.setSize(Math.abs(x1-x0),Math.abs(y1-y0));
        arrawLabel.setLocation(x0,y0);
        return arrawLabel;
    }
}
