package de.Unknown;

import java.util.HashMap;
import java.util.Map;

public class State {
    private String id;
    public Map<String,Integer> inputToNext;

    public State(String identifier){
        id = identifier;
        inputToNext = new HashMap<>();
    }

    public String getID(){
        return id;
    }

}
